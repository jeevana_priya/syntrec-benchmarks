void desugar (srcAST s, ref dstAST _out)/*../bet.sk:52*/
{
  _out = null;
  dstAST ret_s1773 = null;
  bit _has_out_ = 0;
  if(s == (null))/*../bet.sk:6*/
  {
    ret_s1773 = null;
    _has_out_ = 1;
  }
  if(_has_out_ == 0)/*../bet.sk:7*/
  {
    switch(s){/*../bet.sk:7*/
      case NumS:
      {
        int tmp_s32 = s.val;
        dstAST dstAST__s28 = new NumD(val=tmp_s32);
        ret_s1773 = dstAST__s28;
      }
      case NegS:
      {
        opcode opcode__s113 = new Ominus();
        dstAST dstAST__s105_0 = new NumD(val=0);
        int _out_s617 = s.val;
        dstAST dstAST__s105_1 = new NumD(val=_out_s617);
        dstAST dstAST__s105;
        dstAST__s105 = new BinaryD(op=opcode__s113, a=dstAST__s105_0, b=dstAST__s105_1);
        ret_s1773 = dstAST__s105;
      }
      case TrueS:
      {
        dstAST dstAST__s216 = new BoolD(v=1);
        ret_s1773 = dstAST__s216;
      }
      case FalseS:
      {
        dstAST dstAST__s404 = new BoolD(v=0);
        ret_s1773 = dstAST__s404;
      }
      case BinaryS:
      {
        srcAST s_4c5 = s.a;
        srcAST s_4c5_0 = s.b;
        dstAST[2] a_s1783 = ((dstAST[2])null);
        dstAST _out_s1789_0 = null;
        desugar(s_4c5, _out_s1789_0);
        a_s1783[0] = _out_s1789_0;
        dstAST _out_s1789_1 = null;
        desugar(s_4c5_0, _out_s1789_1);
        a_s1783[1] = _out_s1789_1;
        opcode tmp_s806 = s.op;
        dstAST tmp_s811 = a_s1783[0];
        dstAST tmp_s873 = a_s1783[1];
        dstAST dstAST__s793 = new BinaryD(op=tmp_s806, a=tmp_s811, b=tmp_s873);
        ret_s1773 = dstAST__s793;
      }
      case BetweenS:
      {
        srcAST s_599 = s.a;
        srcAST s_599_0 = s.b;
        srcAST s_599_1 = s.c;
        dstAST[3] a_s1785 = ((dstAST[3])null);
        dstAST _out_s1789_2 = null;
        desugar(s_599, _out_s1789_2);
        a_s1785[0] = _out_s1789_2;
        dstAST _out_s1789_3 = null;
        desugar(s_599_0, _out_s1789_3);
        a_s1785[1] = _out_s1789_3;
        dstAST _out_s1789_4 = null;
        desugar(s_599_1, _out_s1789_4);
        a_s1785[2] = _out_s1789_4;
        opcode op_s1006 = new Oand();
        opcode opcode__s1025 = new Ogt();
        dstAST tmp_s1028 = a_s1785[1];
        dstAST tmp_s1043 = a_s1785[0];
        dstAST a_s1009 = new BinaryD(op=opcode__s1025, a=tmp_s1028, b=tmp_s1043);
        opcode opcode__s1087 = new Olt();
        dstAST tmp_s1090 = a_s1785[1];
        dstAST tmp_s1105 = a_s1785[2];
        dstAST b_s1071 = new BinaryD(op=opcode__s1087, a=tmp_s1090, b=tmp_s1105);
        dstAST dstAST__s994 = new BinaryD(op=op_s1006, a=a_s1009, b=b_s1071);
        ret_s1773 = dstAST__s994;
      }
      case IfS:
      {
        srcAST s_66f = s.a;
        srcAST s_66f_0 = s.b;
        srcAST s_66f_1 = s.c;
        dstAST[3] a_s1787 = ((dstAST[3])null);
        dstAST _out_s1789_5 = null;
        desugar(s_66f, _out_s1789_5);
        a_s1787[0] = _out_s1789_5;
        dstAST _out_s1789_6 = null;
        desugar(s_66f_0, _out_s1789_6);
        a_s1787[1] = _out_s1789_6;
        dstAST _out_s1789_7 = null;
        desugar(s_66f_1, _out_s1789_7);
        a_s1787[2] = _out_s1789_7;
        opcode opcode__s1226 = new Oor();
        dstAST tmp_s1229 = a_s1787[0];
        dstAST dstAST__s1246_s1252 = new BoolD(v=0);
        dstAST a_s1210 = new BinaryD(op=opcode__s1226, a=tmp_s1229, b=dstAST__s1246_s1252);
        dstAST tmp_s1275 = a_s1787[1];
        dstAST tmp_s1337 = a_s1787[2];
        dstAST dstAST__s1195 = new IfD(a=a_s1210, b=tmp_s1275, c=tmp_s1337);
        ret_s1773 = dstAST__s1195;
      }
    }
  }
  _out = ret_s1773;
  return;
}
      
      