void unionL (List l1, List l2, ref List _out)/*../lUnion.sk:24*/
{
  _out = null;
  List ret_s86 = null;
  bit _has_out_ = 0;
  if(l2 == (null))/*../lUnion.sk:6*/
  {
    ret_s86 = null;
    _has_out_ = 1;
  }
  if(_has_out_ == 0)/*../lUnion.sk:7*/
  {
    switch(l2){/*../lUnion.sk:7*/
      case Cons:
      {
        List s_28 = l2.tail;
        List[1] a_s88 = ((List[1])null);
        List _out_s106 = null;
        unionL(l1, s_28, _out_s106);
        a_s88[0] = _out_s106;
        int tmp_s13 = l2.head;
        List tmp_s19 = a_s88[0];
        List List__s8 = new Cons(head=tmp_s13, tail=tmp_s19);
        ret_s86 = List__s8;
      }
      case Nil:
      {
        ret_s86 = l1;
      }
    }
  }
  _out = ret_s86;
  return;
}