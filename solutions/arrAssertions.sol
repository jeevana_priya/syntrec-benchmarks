void desugar (srcAST p, Type[4] symtab_0, ref dstAST _out)/*arrAssertions.sk:68*/
{
  _out = null;
  srcAST src = p;
  dstAST _out_s5312 = null;
  bit _has_out_ = 0;
  if(p == (null))/*arrAssertions.sk:40*/
  {
    _out_s5312 = null;
    _has_out_ = 1;
  }
  if(_has_out_ == 0)/*arrAssertions.sk:41*/
  {
    switch(p){/*arrAssertions.sk:41*/
      case VarS:
      {
        int tmp_s836 = p.id;
        dstAST dstAST__s40 = new VarD(id=tmp_s836);
        _out_s5312 = dstAST__s40;
      }
      case NumS:
      {
        int tmp_s1664 = p.val;
        dstAST dstAST__s855 = new NumD(val=tmp_s1664);
        _out_s5312 = dstAST__s855;
      }
      case BinaryS:
      {
        srcAST s_d81 = p.a;
        srcAST s_d81_0 = p.b;
        dstAST[2] a_s5746 = ((dstAST[2])null);
        dstAST _out_s6398 = null;
        desugar(s_d81, symtab_0, _out_s6398);
        a_s5746[0] = _out_s6398;
        dstAST _out_s6398_0 = null;
        desugar(s_d81_0, symtab_0, _out_s6398_0);
        a_s5746[1] = _out_s6398_0;
        dstAST tmp_s1683 = null;
        dstAST tmp_s1966 = a_s5746[0];
        dstAST tmp_s2046 = a_s5746[1];
        Opcode op_s2224 = new Ominus();
        dstAST stmts_s1953 = new BinaryD(op=op_s2224, a=tmp_s1966, b=tmp_s2046);
        dstAST[2] stmts_s1678 = ((dstAST[2]){tmp_s1683,stmts_s1953}[0::2]);
        dstAST tmp_s2244 = a_s5746[0];
        dstAST tmp_s2324 = a_s5746[1];
        Opcode tmp_s2503 = p.op;
        dstAST node_s2231 = new BinaryD(op=tmp_s2503, a=tmp_s2244, b=tmp_s2324);
        dstAST dstAST__s1670 = new BlockD(n=2, stmts=stmts_s1678, node=node_s2231);
        _out_s5312 = dstAST__s1670;
      }
      case ArrAccessS:
      {
        srcAST s_1238 = p.base;
        srcAST s_1238_0 = p.idx;
        dstAST[2] a_s5962 = ((dstAST[2])null);
        dstAST _out_s6398_1 = null;
        desugar(s_1238, symtab_0, _out_s6398_1);
        a_s5962[0] = _out_s6398_1;
        dstAST _out_s6398_2 = null;
        desugar(s_1238_0, symtab_0, _out_s6398_2);
        a_s5962[1] = _out_s6398_2;
        srcAST t = p.base;
        int len_s6394 = 0;
        getLen(t, symtab_0, len_s6394);
        dstAST tmp_s2537 = a_s5962[1];
        dstAST tmp_s2830 = a_s5962[1];
        dstAST dstAST__s2847_s2853 = new NumD(val=len_s6394);
        Opcode Opcode__s2897 = new Olt();
        dstAST stmts_s2818 = new BinaryD(op=Opcode__s2897, a=tmp_s2830, b=dstAST__s2847_s2853);
        dstAST stmts_s2808 = new AssertD(expr=stmts_s2818);
        dstAST[2] stmts_s2533 = ((dstAST[2]){tmp_s2537,stmts_s2808}[0::2]);
        dstAST tmp_s3099 = a_s5962[0];
        dstAST tmp_s3179 = a_s5962[1];
        dstAST node_s3086 = new ArrAccessD(base=tmp_s3099, idx=tmp_s3179);
        dstAST dstAST__s2525 = new BlockD(n=2, stmts=stmts_s2533, node=node_s3086);
        _out_s5312 = dstAST__s2525;
      }
      case ArrInitS:
      {
        srcAST[src.n] __sa6521_16f4 = p.elems[0::p.n];
        int n = p.n;
        dstAST[src.n] newElems_s6180 = ((dstAST[src.n])null);
        bit _has_out__0 = 0;
        if(((n == 0) || (__sa6521_16f4 == (null))) || (__sa6521_16f4 == ({})))/*arrAssertions.sk:65*/
        {
          newElems_s6180 = ((dstAST[src.n]){});
          _has_out__0 = 1;
        }
        dstAST[n] d;
        if(_has_out__0 == 0)/*arrAssertions.sk:67*/
        {
          for(int i = 0; i < n; i = i + 1)/*Canonical*/
          {
            srcAST t_0 = __sa6521_16f4[i];
            dstAST _out_s6398_3 = null;
            desugar(t_0, symtab_0, _out_s6398_3);
            d[i] = _out_s6398_3;
          }
        }
        if(_has_out__0 == 0)/*arrAssertions.sk:70*/
        {
          newElems_s6180 = ((dstAST[src.n])d);
        }
        int tmp_s4220 = p.n;
        dstAST[tmp_s4220] tmp_s4227 = ((p.n) > 0 ? newElems_s6180[0::tmp_s4220] : null);
        dstAST dstAST__s3380 = new ArrInitD(n=tmp_s4220, elems=tmp_s4227);
        _out_s5312 = dstAST__s3380;
      }
    }
  }
  _out = _out_s5312;
  return;
}
      
      
      
