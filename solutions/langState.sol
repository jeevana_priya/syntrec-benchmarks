void desugar (srcAST s, ref dstAST _out)/*../betS.sk:52*/
{
  _out = null;
  dstAST ret_s8121 = null;
  bit _has_out_ = 0;
  if(s == (null))/*../betS.sk:6*/
  {
    ret_s8121 = null;
    _has_out_ = 1;
  }
  if(_has_out_ == 0)/*../betS.sk:7*/
  {
    switch(s){/*../betS.sk:7*/
      case NumS:
      {
        int tmp_s39 = s.val;
        dstAST dstAST__s35 = new NumD(val=tmp_s39);
        ret_s8121 = dstAST__s35;
      }
      case VarS:
      {
        int tmp_s839 = s.str;
        dstAST dstAST__s831 = new VarD(str=tmp_s839);
        ret_s8121 = dstAST__s831;
      }
      case TrueS:
      {
        dstAST dstAST__s1627 = new BoolD(v=1);
        ret_s8121 = dstAST__s1627;
      }
      case FalseS:
      {
        dstAST a_s2446 = new BoolD(v=0);
        dstAST b_s2831 = new VarD(str=2);
        dstAST dstAST__s2423 = new LetD(str=2, a=a_s2446, b=b_s2831);
        ret_s8121 = dstAST__s2423;
      }
      case LetS:
      {
        srcAST s_1320 = s.a;
        srcAST s_1320_0 = s.b;
        dstAST[2] a_s8131 = ((dstAST[2])null);
        dstAST _out_s8139 = null;
        desugar(s_1320, _out_s8139);
        a_s8131[0] = _out_s8139;
        dstAST _out_s8139_0 = null;
        desugar(s_1320_0, _out_s8139_0);
        a_s8131[1] = _out_s8139_0;
        int tmp_s3240 = s.str;
        int tmp_s3265 = s.str;
        dstAST tmp_s3271 = a_s8131[0];
        dstAST tmp_s3457 = a_s8131[1];
        dstAST a_s3243 = new LetD(str=tmp_s3265, a=tmp_s3271, b=tmp_s3457);
        int tmp_s3653 = s.str;
        dstAST b_s3643 = new VarD(str=tmp_s3653);
        dstAST dstAST__s3219 = new LetD(str=tmp_s3240, a=a_s3243, b=b_s3643);
        ret_s8121 = dstAST__s3219;
      }
      case IncS:
      {
        int tmp_s4062 = s.str;
        dstAST dstAST__s4046 = new IncD(str=tmp_s4062);
        ret_s8121 = dstAST__s4046;
      }
      case BetweenS:
      {
        srcAST s_1989 = s.a;
        srcAST s_1989_0 = s.b;
        srcAST s_1989_1 = s.c;
        dstAST[3] a_s8135 = ((dstAST[3])null);
        dstAST _out_s8139_1 = null;
        desugar(s_1989, _out_s8139_1);
        a_s8135[0] = _out_s8139_1;
        dstAST _out_s8139_2 = null;
        desugar(s_1989_0, _out_s8139_2);
        a_s8135[1] = _out_s8139_2;
        dstAST _out_s8139_3 = null;
        desugar(s_1989_1, _out_s8139_3);
        a_s8135[2] = _out_s8139_3;
        dstAST tmp_s4869 = a_s8135[0];
        dstAST tmp_s5294 = a_s8135[1];
        dstAST tmp_s5529 = a_s8135[2];
        dstAST dstAST__s5556_s5562 = new VarD(str=2);
        opcode opcode__s5580 = new Ogt();
        dstAST a_s5502 = new Prim2D(op=opcode__s5580, a=tmp_s5529, b=dstAST__s5556_s5562);
        dstAST dstAST__s5610_s5616 = new VarD(str=2);
        dstAST dstAST__s5635_s5641 = new VarD(str=3);
        opcode opcode__s5659 = new Ogt();
        dstAST b_s5581 = new Prim2D(op=opcode__s5659, a=dstAST__s5610_s5616, b=dstAST__s5635_s5641);
        opcode op_s5660 = new Oand();
        dstAST b_s5477 = new Prim2D(op=op_s5660, a=a_s5502, b=b_s5581);
        dstAST b_s5266 = new LetD(str=2, a=tmp_s5294, b=b_s5477);
        dstAST dstAST__s4842 = new LetD(str=3, a=tmp_s4869, b=b_s5266);
        ret_s8121 = dstAST__s4842;
      }
      case Prim2S:
      {
        srcAST s_1cd1 = s.a;
        srcAST s_1cd1_0 = s.b;
        dstAST[2] a_s8137 = ((dstAST[2])null);
        dstAST _out_s8139_4 = null;
        desugar(s_1cd1, _out_s8139_4);
        a_s8137[0] = _out_s8139_4;
        dstAST _out_s8139_5 = null;
        desugar(s_1cd1_0, _out_s8139_5);
        a_s8137[1] = _out_s8139_5;
        dstAST tmp_s5696 = a_s8137[0];
        dstAST tmp_s6096 = a_s8137[1];
        opcode tmp_s6494 = s.op;
        dstAST dstAST__s5669 = new Prim2D(op=tmp_s6494, a=tmp_s5696, b=tmp_s6096);
        ret_s8121 = dstAST__s5669;
      }
    }
  }
  _out = ret_s8121;
  return;
}
      
      
