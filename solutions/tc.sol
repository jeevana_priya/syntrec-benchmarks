void getConstraints (E e, Type t, Type[5] envt_0, ref int id, ref Constraint _out)/*../tc.sk:47*/
{
  _out = null;
  Type[5] envt = envt_0;
  int uo_s10 = id;
  id = id + 1;
  Unknown u1 = new Unknown(id=uo_s10);
  int uo_s11 = id;
  id = id + 1;
  Unknown u2 = new Unknown(id=uo_s11);
  Constraint _out_s1024 = null;
  bit _has_out_ = 0;
  if(e == (null))/*../tc.sk:6*/
  {
    _out_s1024 = null;
    _has_out_ = 1;
  }
  Type t1 = null;
  if(_has_out_ == 0)/*../tc.sk:7*/
  {
    switch(e){/*../tc.sk:7*/
      case Num:
      {
        Type l_s15 = new Int();
        Constraint Constraint__s13 = new Equals(l=l_s15, r=t);
        _out_s1024 = Constraint__s13;
      }
      case Var:
      {
        int v = e.name;
        t1 = envt_0[v];
        Constraint Constraint__s187 = new Equals(l=t1, r=t);
        _out_s1024 = Constraint__s187;
      }
      case Abs:
      {
        int v_0 = e.var;
        envt[v_0] = u2;
        E s_3c5 = e.a;
        Constraint[1] a_s1102 = ((Constraint[1])null);
        Constraint _out_s1180 = null;
        getConstraints(s_3c5, u1, envt, id, _out_s1180);
        a_s1102[0] = _out_s1180;
        Constraint tmp_s449 = a_s1102[0];
        Type Type__s519 = new Function(in=u2, out=u1);
        Constraint b_s494 = new Equals(l=t, r=Type__s519);
        Constraint Constraint__s361 = new And(a=tmp_s449, b=b_s494);
        _out_s1024 = Constraint__s361;
      }
      case App:
      {
        E s_55f = e.a;
        E s_55f_0 = e.b;
        Constraint[2] a_s1140 = ((Constraint[2])null);
        Type Type__s723 = new Function(in=u2, out=t);
        Constraint _out_s1180_0 = null;
        getConstraints(s_55f, Type__s723, envt_0, id, _out_s1180_0);
        a_s1140[0] = _out_s1180_0;
        Constraint _out_s1180_1 = null;
        getConstraints(s_55f_0, u2, envt_0, id, _out_s1180_1);
        a_s1140[1] = _out_s1180_1;
        Constraint tmp_s630 = a_s1140[0];
        Constraint tmp_s678 = a_s1140[1];
        Constraint Constraint__s542 = new And(a=tmp_s630, b=tmp_s678);
        _out_s1024 = Constraint__s542;
      }
    }
  }
  _out = _out_s1024;
  return;
}
  