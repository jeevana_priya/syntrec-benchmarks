void desugar (srcAST s, ref dstAST _out)/*lcB.sk:32*/
{
  _out = null;
  dstAST _out_s102 = null;
  bit _has_out_ = 0;
  if(s == (null))/*lcB.sk:12*/
  {
    _out_s102 = null;
    _has_out_ = 1;
  }
  if(_has_out_ == 0)/*lcB.sk:13*/
  {
    switch(s){/*lcB.sk:13*/
      case AndS:
      {
        srcAST s_3b = s.a;
        srcAST s_3b_0 = s.b;
        dstAST[2] a_s165 = ((dstAST[2])null);
        dstAST _out_s373 = null;
        desugar(s_3b, _out_s373);
        a_s165[0] = _out_s373;
        dstAST _out_s373_0 = null;
        desugar(s_3b_0, _out_s373_0);
        a_s165[1] = _out_s373_0;
        dstAST _pac_sc_s354 = a_s165[1];
        dstAST _pac_sc_s354_0 = a_s165[0];
        dstAST dstAST__s64_0 = new AppD(a=_pac_sc_s354, b=_pac_sc_s354_0);
        dstAST _pac_sc_s354_1 = a_s165[1];
        dstAST dstAST__s64;
        dstAST__s64 = new AppD(a=dstAST__s64_0, b=_pac_sc_s354_1);
        _out_s102 = dstAST__s64;
      }
      case OrS:
      {
        srcAST s_6c9 = s.a;
        srcAST s_6c9_0 = s.b;
        dstAST[2] a_s179 = ((dstAST[2])null);
        dstAST _out_s373_1 = null;
        desugar(s_6c9, _out_s373_1);
        a_s179[0] = _out_s373_1;
        dstAST _out_s373_2 = null;
        desugar(s_6c9_0, _out_s373_2);
        a_s179[1] = _out_s373_2;
        dstAST _pac_sc_s326 = a_s179[1];
        dstAST _pac_sc_s326_0 = a_s179[1];
        dstAST dstAST__s55_0 = new AppD(a=_pac_sc_s326, b=_pac_sc_s326_0);
        dstAST _pac_sc_s326_1 = a_s179[0];
        dstAST dstAST__s55;
        dstAST__s55 = new AppD(a=dstAST__s55_0, b=_pac_sc_s326_1);
        _out_s102 = dstAST__s55;
      }
      case NotS:
      {
        srcAST s_d57 = s.a;
        dstAST[1] a_s193 = ((dstAST[1])null);
        dstAST _out_s373_3 = null;
        desugar(s_d57, _out_s373_3);
        a_s193[0] = _out_s373_3;
        dstAST _pac_sc_s298 = a_s193[0];
        VarD VarD__s52 = new VarD(name=0);
        VarD VarD__s52_0 = new VarD(name=1);
        dstAST dstAST__s46_3 = new VarD(name=1);
        dstAST dstAST__s46_2 = new AbsD(var=VarD__s52_0, a=dstAST__s46_3);
        dstAST dstAST__s46_1 = new AbsD(var=VarD__s52, a=dstAST__s46_2);
        dstAST dstAST__s46_0 = new AppD(a=_pac_sc_s298, b=dstAST__s46_1);
        VarD VarD__s52_1 = new VarD(name=0);
        VarD VarD__s52_2 = new VarD(name=1);
        dstAST dstAST__s46_6 = new VarD(name=0);
        dstAST dstAST__s46_5 = new AbsD(var=VarD__s52_2, a=dstAST__s46_6);
        dstAST dstAST__s46_4 = new AbsD(var=VarD__s52_1, a=dstAST__s46_5);
        dstAST dstAST__s46;
        dstAST__s46 = new AppD(a=dstAST__s46_0, b=dstAST__s46_4);
        _out_s102 = dstAST__s46;
      }
      case TrueS:
      {
        VarD VarD__s43 = new VarD(name=1);
        VarD VarD__s43_0 = new VarD(name=0);
        dstAST dstAST__s37_1 = new VarD(name=1);
        dstAST dstAST__s37_0 = new AbsD(var=VarD__s43_0, a=dstAST__s37_1);
        dstAST dstAST__s37;
        dstAST__s37 = new AbsD(var=VarD__s43, a=dstAST__s37_0);
        _out_s102 = dstAST__s37;
      }
      case FalseS:
      {
        VarD VarD__s34 = new VarD(name=1);
        VarD VarD__s34_0 = new VarD(name=0);
        dstAST dstAST__s28_1 = new VarD(name=0);
        dstAST dstAST__s28_0 = new AbsD(var=VarD__s34_0, a=dstAST__s28_1);
        dstAST dstAST__s28;
        dstAST__s28 = new AbsD(var=VarD__s34, a=dstAST__s28_0);
        _out_s102 = dstAST__s28;
      }
    }
  }
  _out = _out_s102;
  return;
}
      
