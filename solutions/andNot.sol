void optimize (AST original, int[2] assignment, ref AST _out)/*../andNot.sk:16*/
{
  _out = null;
  AST _out_s269 = null;
  AST AST__s1 = new Var(id=1, type=1);
  int[32] tmp = {16,1,18,1,0,16,3,8,8,17,0,8,8,2,8,0,18,0,8,16,22,2,20,8,4,17,16,4,24,24,24,24};
  bit _out_s271 = 0;
  check(2, AST__s1, tmp[0::2], _out_s271);
  assert (_out_s271); //Assert at ../andNot.sk:101 (-2467741152241529324)
  bit _out_s273 = 0;
  check(2, AST__s1, assignment, _out_s273);
  bit _has_out_ = 0;
  if(_out_s273)/*../andNot.sk:103*/
  {
    AST a_s106 = new Bool(x=1);
    AST AST__s105 = new Not(a=a_s106);
    int _out_s275 = 0;
    count(AST__s105, _out_s275);
    int _out_s277 = 0;
    count(original, _out_s277);
    assert (_out_s275 < _out_s277); //Assert at ../andNot.sk:105 (2490494145897318095)
    _out_s269 = AST__s105;
    _has_out_ = 1;
  }
  if(_has_out_ == 0)/*../andNot.sk:108*/
  {
    _out_s269 = original;
  }
  _out = _out_s269;
  return;
}
