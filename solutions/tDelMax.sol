void deleteMax (BinaryTree tree, ref BinaryTree _out)/*../tDelMax.sk:26*/
{
  _out = null;
  BinaryTree ret_s331 = null;
  bit _has_out_ = 0;
  if(tree == (null))/*../tDelMax.sk:6*/
  {
    ret_s331 = null;
    _has_out_ = 1;
  }
  if(_has_out_ == 0)/*../tDelMax.sk:7*/
  {
    switch(tree){/*../tDelMax.sk:7*/
      case Branch:
      {
        BinaryTree s_1f = tree.l;
        BinaryTree s_1f_0 = tree.r;
        BinaryTree[2] a_s333 = ((BinaryTree[2])null);
        BinaryTree _out_s347 = null;
        deleteMax(s_1f, _out_s347);
        a_s333[0] = _out_s347;
        BinaryTree _out_s347_0 = null;
        deleteMax(s_1f_0, _out_s347_0);
        a_s333[1] = _out_s347_0;
        bit _out_s406 = 0;
        isEmpty(tree.r, _out_s406);
        if(_out_s406)/*../tDelMax.sk:13*/
        {
          BinaryTree tmp_s16 = tree.l;
          ret_s331 = tmp_s16;
          _has_out_ = 1;
        }
        bit _out_s335 = 0;
        if(_has_out_ == 0)/*../tDelMax.sk:13*/
        {
          _out_s335 = 1;
        }
        if(_has_out_ == 0)/*../tDelMax.sk:13*/
        {
          if(_out_s335)/*../tDelMax.sk:13*/
          {
            int tmp_s20 = tree.value;
            BinaryTree tmp_s25 = tree.l;
            BinaryTree tmp_s61 = a_s333[1];
            BinaryTree BinaryTree__s15 = new Branch(value=tmp_s20, l=tmp_s25, r=tmp_s61);
            ret_s331 = BinaryTree__s15;
          }
        }
      }
      case Leaf:
      {
        BinaryTree BinaryTree__s97 = new Empty();
        ret_s331 = BinaryTree__s97;
      }
      case Empty:
      {
        BinaryTree BinaryTree__s172 = new Empty();
        ret_s331 = BinaryTree__s172;
      }
    }
  }
  _out = ret_s331;
  return;
}
            
