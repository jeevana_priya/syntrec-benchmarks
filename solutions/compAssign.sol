void desugar (srcExpr e, ref dstExpr _out)/*compAssign.sk:42*/
{
  _out = null;
  dstExpr _out_s161 = null;
  bit _has_out_ = 0;
  if(e == (null))/*compAssign.sk:12*/
  {
    _out_s161 = null;
    _has_out_ = 1;
  }
  if(_has_out_ == 0)/*compAssign.sk:13*/
  {
    switch(e){/*compAssign.sk:13*/
      case AssignS:
      {
        srcExpr s_3e = e.a;
        dstExpr[1] a_s255 = ((dstExpr[1])null);
        dstExpr _out_s561 = null;
        desugar(s_3e, _out_s561);
        a_s255[0] = _out_s561;
        int _out_s556 = e.var;
        dstExpr _pac_sc_s544 = a_s255[0];
        dstExpr dstExpr__s119 = new AssignD(var=_out_s556, a=_pac_sc_s544);
        _out_s161 = dstExpr__s119;
      }
      case IncrAssignS:
      {
        srcExpr s_263 = e.a;
        dstExpr[1] a_s269 = ((dstExpr[1])null);
        dstExpr _out_s561_0 = null;
        desugar(s_263, _out_s561_0);
        a_s269[0] = _out_s561_0;
        int _out_s526 = e.var;
        int _out_s526_0 = e.var;
        dstExpr dstExpr__s109_1 = new VarD(id=_out_s526_0);
        opcode opcode__s117 = new Oplus();
        dstExpr _pac_sc_s514 = a_s269[0];
        dstExpr dstExpr__s109_0 = new BinaryD(op=opcode__s117, a=dstExpr__s109_1, b=_pac_sc_s514);
        dstExpr dstExpr__s109;
        dstExpr__s109 = new AssignD(var=_out_s526, a=dstExpr__s109_0);
        _out_s161 = dstExpr__s109;
        }
      case DecrAssignS:
      {
        srcExpr s_488 = e.a;
        dstExpr[1] a_s283 = ((dstExpr[1])null);
        dstExpr _out_s561_1 = null;
        desugar(s_488, _out_s561_1);
        a_s283[0] = _out_s561_1;
        int _out_s496 = e.var;
        int _out_s496_0 = e.var;
        dstExpr dstExpr__s99_1 = new VarD(id=_out_s496_0);
        opcode opcode__s107 = new Ominus();
        dstExpr _pac_sc_s484 = a_s283[0];
        dstExpr dstExpr__s99_0 = new BinaryD(op=opcode__s107, a=dstExpr__s99_1, b=_pac_sc_s484);
        dstExpr dstExpr__s99;
        dstExpr__s99 = new AssignD(var=_out_s496, a=dstExpr__s99_0);
        _out_s161 = dstExpr__s99;
      }
      case IncrOneS:
      {
        int _out_s464 = e.var;
        int _out_s464_0 = e.var;
        dstExpr dstExpr__s89_1 = new VarD(id=_out_s464_0);
        opcode opcode__s97 = new Oplus();
        dstExpr dstExpr__s89_2 = new NumD(val=1);
        dstExpr dstExpr__s89_0 = new BinaryD(op=opcode__s97, a=dstExpr__s89_1, b=dstExpr__s89_2);
        dstExpr dstExpr__s89;
        dstExpr__s89 = new AssignD(var=_out_s464, a=dstExpr__s89_0);
        _out_s161 = dstExpr__s89;
      }
      case VarS:
      {
        int _out_s434 = e.id;
        dstExpr dstExpr__s79 = new VarD(id=_out_s434);
        _out_s161 = dstExpr__s79;
      }
      case NumS:
      {
        int _out_s404 = e.val;
        dstExpr dstExpr__s69 = new NumD(val=_out_s404);
        _out_s161 = dstExpr__s69;
      }
      case BinaryS:
      {
        srcExpr s_d16 = e.a;
        srcExpr s_d16_0 = e.b;
        dstExpr[2] a_s339 = ((dstExpr[2])null);
        dstExpr _out_s561_2 = null;
        desugar(s_d16, _out_s561_2);
        a_s339[0] = _out_s561_2;
        dstExpr _out_s561_3 = null;
        desugar(s_d16_0, _out_s561_3);
        a_s339[1] = _out_s561_3;
        dstExpr _pac_sc_s362 = a_s339[0];
        opcode _out_s369 = e.op;
        dstExpr _pac_sc_s362_0 = a_s339[1];
        dstExpr dstExpr__s59 = new BinaryD(op=_out_s369, a=_pac_sc_s362, b=_pac_sc_s362_0);
        _out_s161 = dstExpr__s59;
      }
    }
  }
  _out = _out_s161;
  return;
}
      