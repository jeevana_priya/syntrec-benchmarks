void desugar (srcAST s, ref dstAST _out)/*mergeOp.sk:60*/
{
  _out = null;
  dstAST _out_s316 = null;
  bit _has_out_ = 0;
  if(s == (null))/*mergeOp.sk:12*/
  {
    _out_s316 = null;
    _has_out_ = 1;
  }
  if(_has_out_ == 0)/*mergeOp.sk:13*/
  {
    switch(s){/*mergeOp.sk:13*/
      case NumS:
      {
        opcode opcode__s237 = new Oneg();
        opcode opcode__s237_0 = new Oneg();
        int _out_s1313 = s.val;
        dstAST dstAST__s230_1 = new NumD(val=_out_s1313);
        dstAST dstAST__s230_0 = new UnaryD(op=opcode__s237_0, a=dstAST__s230_1);
        dstAST dstAST__s230;
        dstAST__s230 = new UnaryD(op=opcode__s237, a=dstAST__s230_0);
        _out_s316 = dstAST__s230;
      }
      case TrueS:
      {
        dstAST dstAST__s221 = new BoolD(v=1);
        _out_s316 = dstAST__s221;
      }
      case FalseS:
      {
        opcode opcode__s219 = new Oor();
        opcode opcode__s219_0 = new Onot();
        dstAST dstAST__s212_1 = new BoolD(v=1);
        dstAST dstAST__s212_0 = new UnaryD(op=opcode__s219_0, a=dstAST__s212_1);
        dstAST dstAST__s212_2 = new BoolD(v=0);
        dstAST dstAST__s212;
        dstAST__s212 = new BinaryD(op=opcode__s219, a=dstAST__s212_0, b=dstAST__s212_2);
        _out_s316 = dstAST__s212;
      }
      case NegS:
      {
        opcode opcode__s210 = new Oneg();
        int _out_s1205 = s.val;
        dstAST dstAST__s203_0 = new NumD(val=_out_s1205);
        dstAST dstAST__s203;
        dstAST__s203 = new UnaryD(op=opcode__s210, a=dstAST__s203_0);
        _out_s316 = dstAST__s203;
      }
      case NotS:
      {
        opcode opcode__s201 = new Onot();
        bit _out_s1161 = s.v;
        dstAST dstAST__s194_0 = new BoolD(v=_out_s1161);
        dstAST dstAST__s194;
        dstAST__s194 = new UnaryD(op=opcode__s201, a=dstAST__s194_0);
        _out_s316 = dstAST__s194;
      }
      case DoubleS:
      {
        opcode opcode__s192 = new Oplus();
        int _out_s1133 = s.val;
        dstAST dstAST__s185_0 = new NumD(val=_out_s1133);
        int _out_s1133_0 = s.val;
        dstAST dstAST__s185_1 = new NumD(val=_out_s1133_0);
        dstAST dstAST__s185;
        dstAST__s185 = new BinaryD(op=opcode__s192, a=dstAST__s185_0, b=dstAST__s185_1);
        _out_s316 = dstAST__s185;
      }
      case TripleS:
      {
        opcode opcode__s183 = new Oplus();
        int _out_s1097 = s.val;
        dstAST dstAST__s176_0 = new NumD(val=_out_s1097);
        opcode opcode__s183_0 = new Oplus();
        int _out_s1097_0 = s.val;
        dstAST dstAST__s176_2 = new NumD(val=_out_s1097_0);
        int _out_s1097_1 = s.val;
        dstAST dstAST__s176_3 = new NumD(val=_out_s1097_1);
        dstAST dstAST__s176_1 = new BinaryD(op=opcode__s183_0, a=dstAST__s176_2, b=dstAST__s176_3);
        dstAST dstAST__s176;
        dstAST__s176 = new BinaryD(op=opcode__s183, a=dstAST__s176_0, b=dstAST__s176_1);
        _out_s316 = dstAST__s176;
      }
      case PlusS:
      {
        srcAST s_c68 = s.a;
        srcAST s_c68_0 = s.b;
        dstAST[2] a_s668 = ((dstAST[2])null);
        dstAST _out_s1064 = null;
        desugar(s_c68, _out_s1064);
        a_s668[0] = _out_s1064;
        dstAST _out_s1064_0 = null;
        desugar(s_c68_0, _out_s1064_0);
        a_s668[1] = _out_s1064_0;
        opcode opcode__s174 = new Ominus();
        opcode opcode__s174_0 = new Oplus();
        dstAST _pac_sc_s1039 = a_s668[0];
        dstAST _pac_sc_s1039_0 = a_s668[1];
        dstAST dstAST__s167_0 = new BinaryD(op=opcode__s174_0, a=_pac_sc_s1039, b=_pac_sc_s1039_0);
        dstAST dstAST__s167_1 = new NumD(val=0);
        dstAST dstAST__s167;
        dstAST__s167 = new BinaryD(op=opcode__s174, a=dstAST__s167_0, b=dstAST__s167_1);
        _out_s316 = dstAST__s167;
      } // 58
      case MinusS:
      {
        srcAST s_e26 = s.a;
        srcAST s_e26_0 = s.b;
        dstAST[2] a_s682 = ((dstAST[2])null);
        dstAST _out_s1064_1 = null;
        desugar(s_e26, _out_s1064_1);
        a_s682[0] = _out_s1064_1;
        dstAST _out_s1064_2 = null;
        desugar(s_e26_0, _out_s1064_2);
        a_s682[1] = _out_s1064_2;
        opcode opcode__s165 = new Ominus();
        opcode opcode__s165_0 = new Oplus();
        dstAST _pac_sc_s1003 = a_s682[0];
        dstAST dstAST__s158_1 = new NumD(val=1);
        dstAST dstAST__s158_0 = new BinaryD(op=opcode__s165_0, a=_pac_sc_s1003, b=dstAST__s158_1);
        opcode opcode__s165_1 = new Oplus();
        dstAST _pac_sc_s1003_0 = a_s682[1];
        dstAST dstAST__s158_3 = new NumD(val=1);
        dstAST dstAST__s158_2 = new BinaryD(op=opcode__s165_1, a=_pac_sc_s1003_0, b=dstAST__s158_3);
        dstAST dstAST__s158;
        dstAST__s158 = new BinaryD(op=opcode__s165, a=dstAST__s158_0, b=dstAST__s158_2);
        _out_s316 = dstAST__s158;
      }
      case AndS:
      {
        srcAST s_fe4 = s.a;
        srcAST s_fe4_0 = s.b;
        dstAST[2] a_s696 = ((dstAST[2])null);
        dstAST _out_s1064_3 = null;
        desugar(s_fe4, _out_s1064_3);
        a_s696[0] = _out_s1064_3;
        dstAST _out_s1064_4 = null;
        desugar(s_fe4_0, _out_s1064_4);
        a_s696[1] = _out_s1064_4;
        opcode opcode__s156 = new Oor();
        opcode opcode__s156_0 = new Oand();
        dstAST _pac_sc_s967 = a_s696[1];
        dstAST _pac_sc_s967_0 = a_s696[0];
        dstAST dstAST__s149_0 = new BinaryD(op=opcode__s156_0, a=_pac_sc_s967, b=_pac_sc_s967_0);
        dstAST dstAST__s149_1 = new BoolD(v=0);
        dstAST dstAST__s149;
        dstAST__s149 = new BinaryD(op=opcode__s156, a=dstAST__s149_0, b=dstAST__s149_1);
        _out_s316 = dstAST__s149;
      }
      case OrS:
      {
        srcAST s_11a2 = s.a;
        srcAST s_11a2_0 = s.b;
        dstAST[2] a_s710 = ((dstAST[2])null);
        dstAST _out_s1064_5 = null;
        desugar(s_11a2, _out_s1064_5);
        a_s710[0] = _out_s1064_5;
        dstAST _out_s1064_6 = null;
        desugar(s_11a2_0, _out_s1064_6);
        a_s710[1] = _out_s1064_6;
        opcode opcode__s147 = new Oor();
        dstAST _pac_sc_s931 = a_s710[1];
        dstAST _pac_sc_s931_0 = a_s710[0];
        dstAST dstAST__s140 = new BinaryD(op=opcode__s147, a=_pac_sc_s931, b=_pac_sc_s931_0);
        _out_s316 = dstAST__s140;
      }
      case LtS:
      {
        srcAST s_1360 = s.a;
        srcAST s_1360_0 = s.b;
        dstAST[2] a_s724 = ((dstAST[2])null);
        dstAST _out_s1064_7 = null;
        desugar(s_1360, _out_s1064_7);
        a_s724[0] = _out_s1064_7;
        dstAST _out_s1064_8 = null;
        desugar(s_1360_0, _out_s1064_8);
        a_s724[1] = _out_s1064_8;
        opcode opcode__s138 = new Olt();
        dstAST _pac_sc_s895 = a_s724[0];
        dstAST _pac_sc_s895_0 = a_s724[1];
        dstAST dstAST__s131 = new BinaryD(op=opcode__s138, a=_pac_sc_s895, b=_pac_sc_s895_0);
        _out_s316 = dstAST__s131;
      }
      case GtS:
      {
        srcAST s_151e = s.a;
        srcAST s_151e_0 = s.b;
        dstAST[2] a_s738 = ((dstAST[2])null);
        dstAST _out_s1064_9 = null;
        desugar(s_151e, _out_s1064_9);
        a_s738[0] = _out_s1064_9;
        dstAST _out_s1064_10 = null;
        desugar(s_151e_0, _out_s1064_10);
        a_s738[1] = _out_s1064_10;
        opcode opcode__s129 = new Ogt();
        dstAST _pac_sc_s859 = a_s738[0];
        dstAST _pac_sc_s859_0 = a_s738[1];
        dstAST dstAST__s122 = new BinaryD(op=opcode__s129, a=_pac_sc_s859, b=_pac_sc_s859_0);
        _out_s316 = dstAST__s122;
      } // 102
      case EqS:
      {
        srcAST s_16dc = s.a;
        srcAST s_16dc_0 = s.b;
        dstAST[2] a_s752 = ((dstAST[2])null);
        dstAST _out_s1064_11 = null;
        desugar(s_16dc, _out_s1064_11);
        a_s752[0] = _out_s1064_11;
        dstAST _out_s1064_12 = null;
        desugar(s_16dc_0, _out_s1064_12);
        a_s752[1] = _out_s1064_12;
        opcode opcode__s120 = new Oeq();
        opcode opcode__s120_0 = new Oneg();
        dstAST _pac_sc_s823 = a_s752[0];
        dstAST dstAST__s113_0 = new UnaryD(op=opcode__s120_0, a=_pac_sc_s823);
        opcode opcode__s120_1 = new Oneg();
        dstAST _pac_sc_s823_0 = a_s752[1];
        dstAST dstAST__s113_1 = new UnaryD(op=opcode__s120_1, a=_pac_sc_s823_0);
        dstAST dstAST__s113;
        dstAST__s113 = new BinaryD(op=opcode__s120, a=dstAST__s113_0, b=dstAST__s113_1);
        _out_s316 = dstAST__s113;
      }
      case BetweenS:
      {
        srcAST s_189a = s.a;
        srcAST s_189a_0 = s.b;
        srcAST s_189a_1 = s.c;
        dstAST[3] a_s766 = ((dstAST[3])null);
        dstAST _out_s1064_13 = null;
        desugar(s_189a, _out_s1064_13);
        a_s766[0] = _out_s1064_13;
        dstAST _out_s1064_14 = null;
        desugar(s_189a_0, _out_s1064_14);
        a_s766[1] = _out_s1064_14;
        dstAST _out_s1064_15 = null;
        desugar(s_189a_1, _out_s1064_15);
        a_s766[2] = _out_s1064_15;
        opcode opcode__s111 = new Oand();
        opcode opcode__s111_0 = new Ogt();
        dstAST _pac_sc_s787 = a_s766[1];
        dstAST _pac_sc_s787_0 = a_s766[0];
        dstAST dstAST__s104_0 = new BinaryD(op=opcode__s111_0, a=_pac_sc_s787, b=_pac_sc_s787_0);
        opcode opcode__s111_1 = new Olt();
        dstAST _pac_sc_s787_1 = a_s766[1];
        dstAST _pac_sc_s787_2 = a_s766[2];
        dstAST dstAST__s104_1 = new BinaryD(op=opcode__s111_1, a=_pac_sc_s787_1, b=_pac_sc_s787_2);
        dstAST dstAST__s104;
        dstAST__s104 = new BinaryD(op=opcode__s111, a=dstAST__s104_0, b=dstAST__s104_1);
        _out_s316 = dstAST__s104;
      }
    }
  }
  _out = _out_s316;
  return;
}
                         
