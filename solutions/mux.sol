void optimize (AST original, int[3] assignment, ref AST _out)/*../mux.sk:16*/
{
  _out = null;
  AST _out_s269 = null;
  AST AST__s1 = new Var(id=2, type=1);
  int[32] tmp = {8,16,1,16,2,12,17,16,8,24,24,24,24,24,24,24,24,24,24,24,12,24,24,24,24,24,24,18,24,24,24,24};
  bit _out_s271 = 0;
  check(3, AST__s1, tmp[0::3], _out_s271);
  assert (_out_s271); //Assert at ../mux.sk:101 (-15720568597744331)
  bit _out_s273 = 0;
  check(3, AST__s1, assignment, _out_s273);
  bit _has_out_ = 0;
  if(_out_s273)/*../mux.sk:103*/
  {
    AST AST__s105 = new Var(id=1, type=0);
    int _out_s275 = 0;
    count(AST__s105, _out_s275);
    int _out_s277 = 0;
    count(original, _out_s277);
    assert (_out_s275 < _out_s277); //Assert at ../mux.sk:105 (-8664499142508173844)
    _out_s269 = AST__s105;
    _has_out_ = 1;
  }
  if(_has_out_ == 0)/*../mux.sk:108*/
  {
    _out_s269 = original;
  }
  _out = _out_s269;
  return;
}