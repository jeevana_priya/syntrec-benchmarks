void desugar (srcAST s, ref dstAST _out)/*lcP.sk:33*/
{
  _out = null;
  dstAST _out_s90 = null;
  bit _has_out_ = 0;
  if(s == (null))/*lcP.sk:12*/
  {
    _out_s90 = null;
    _has_out_ = 1;
  }
  if(_has_out_ == 0)/*lcP.sk:13*/
  {
    switch(s){/*lcP.sk:13*/
      case NumS:
      {
        int _out_s303 = s.val;
        dstAST dstAST__s54 = new NumD(val=_out_s303);
        _out_s90 = dstAST__s54;
      }
	  case PairS:
      {
        srcAST s_446 = s.a;
        srcAST s_446_0 = s.b;
        dstAST[2] a_s148 = ((dstAST[2])null);
        dstAST _out_s276 = null;
        desugar(s_446, _out_s276);
        a_s148[0] = _out_s276;
        dstAST _out_s276_0 = null;
        desugar(s_446_0, _out_s276_0);
        a_s148[1] = _out_s276_0;
        VarD VarD__s51 = new VarD(name=1);
        dstAST dstAST__s44_2 = new VarD(name=1);
        dstAST _pac_sc_s257 = a_s148[0];
        dstAST dstAST__s44_1 = new AppD(a=dstAST__s44_2, b=_pac_sc_s257);
        dstAST _pac_sc_s257_0 = a_s148[1];
        dstAST dstAST__s44_0 = new AppD(a=dstAST__s44_1, b=_pac_sc_s257_0);
        dstAST dstAST__s44;
        dstAST__s44 = new AbsD(var=VarD__s51, a=dstAST__s44_0);
        _out_s90 = dstAST__s44;
      }
	  case FirstS:
      {
        srcAST s_85d = s.a;
        dstAST[1] a_s162 = ((dstAST[1])null);
        dstAST _out_s276_1 = null;
        desugar(s_85d, _out_s276_1);
        a_s162[0] = _out_s276_1;
        dstAST _pac_sc_s227 = a_s162[0];
        VarD VarD__s41 = new VarD(name=1);
        VarD VarD__s41_0 = new VarD(name=0);
        dstAST dstAST__s34_2 = new VarD(name=1);
        dstAST dstAST__s34_1 = new AbsD(var=VarD__s41_0, a=dstAST__s34_2);
        dstAST dstAST__s34_0 = new AbsD(var=VarD__s41, a=dstAST__s34_1);
        dstAST dstAST__s34;
        dstAST__s34 = new AppD(a=_pac_sc_s227, b=dstAST__s34_0);
        _out_s90 = dstAST__s34;
      }
      case SecondS:
      {
        srcAST s_c72 = s.a;
        dstAST[1] a_s176 = ((dstAST[1])null);
        dstAST _out_s276_2 = null;
        desugar(s_c72, _out_s276_2);
        a_s176[0] = _out_s276_2;
        dstAST _pac_sc_s197 = a_s176[0];
        VarD VarD__s31 = new VarD(name=1);
        VarD VarD__s31_0 = new VarD(name=1);
        dstAST dstAST__s24_2 = new VarD(name=1);
        dstAST dstAST__s24_1 = new AbsD(var=VarD__s31_0, a=dstAST__s24_2);
        dstAST dstAST__s24_0 = new AbsD(var=VarD__s31, a=dstAST__s24_1);
        dstAST dstAST__s24;
        dstAST__s24 = new AppD(a=_pac_sc_s197, b=dstAST__s24_0);
        _out_s90 = dstAST__s24;
      }
	  }
  }
  _out = _out_s90;
  return;
}
	  