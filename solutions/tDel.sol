void deleteNode (BinaryTree tree, int x, ref BinaryTree _out)/*../tDel.sk:26*/
{
  _out = null;
  BinaryTree _out_s333 = null;
  bit _has_out_ = 0;
  if(tree == (null))/*../tDel.sk:6*/
  {
    _out_s333 = null;
    _has_out_ = 1;
  }
  int min = 0;
  if(_has_out_ == 0)/*../tDel.sk:7*/
  {
    switch(tree){/*../tDel.sk:7*/
      case Branch:
      {
        BinaryTree s_28 = tree.l;
        BinaryTree s_28_0 = tree.r;
        BinaryTree[2] a_s335 = ((BinaryTree[2])null);
        BinaryTree _out_s455 = null;
        deleteNode(s_28, x, _out_s455);
        a_s335[0] = _out_s455;
        BinaryTree _out_s455_0 = null;
        deleteNode(s_28_0, x, _out_s455_0);
        a_s335[1] = _out_s455_0;
        bit _pac_sc_s512 = x > (tree.value);
        if(_pac_sc_s512)/*../tDel.sk:27*/
        {
          _pac_sc_s512 = 1;
        }
        if(_pac_sc_s512)/*../tDel.sk:13*/
        {
          int tmp_s20 = tree.value;
          BinaryTree tmp_s25 = tree.l;
          BinaryTree tmp_s61 = a_s335[1];
          BinaryTree BinaryTree__s15 = new Branch(value=tmp_s20, l=tmp_s25, r=tmp_s61);
          _out_s333 = BinaryTree__s15;
          _has_out_ = 1;
        }
        bit _out_s337 = 0;
        if(_has_out_ == 0)/*../tDel.sk:13*/
        {
          bit _pac_sc_s512_0 = x < (tree.value);
          if(_pac_sc_s512_0)/*../tDel.sk:27*/
          {
            _pac_sc_s512_0 = 1;
          }
          _out_s337 = _pac_sc_s512_0;
        }
        if(_has_out_ == 0)/*../tDel.sk:13*/
        {
          if(_out_s337)/*../tDel.sk:13*/
          {
            int tmp_s20_0 = tree.value;
            BinaryTree tmp_s26 = a_s335[0];
            BinaryTree tmp_s60 = tree.r;
            BinaryTree BinaryTree__s15_0 = new Branch(value=tmp_s20_0, l=tmp_s26, r=tmp_s60);
            _out_s333 = BinaryTree__s15_0;
            _has_out_ = 1;
          }
        }
        bit _out_s337_0 = 0;
        if(_has_out_ == 0)/*../tDel.sk:13*/
        {
          bit _pac_sc_s512_1 = x <= (tree.value);
          if(_pac_sc_s512_1)/*../tDel.sk:27*/
          {
            bit _pac_sc_s512_s515 = 0;
            isEmpty(tree.r, _pac_sc_s512_s515);
            _pac_sc_s512_1 = _pac_sc_s512_s515;
          }
          _out_s337_0 = _pac_sc_s512_1;
        }
        if(_has_out_ == 0)/*../tDel.sk:13*/
        {
          if(_out_s337_0)/*../tDel.sk:13*/
          {
            BinaryTree tmp_s16 = tree.l;
            _out_s333 = tmp_s16;
            _has_out_ = 1;
          }
        }
        bit _out_s337_1 = 0;
        if(_has_out_ == 0)/*../tDel.sk:13*/
        {
          bit _pac_sc_s512_2 = x >= (tree.value);
          if(_pac_sc_s512_2)/*../tDel.sk:27*/
          {
            _pac_sc_s512_2 = 1;
          }
          _out_s337_1 = _pac_sc_s512_2;
        }
        if(_has_out_ == 0)/*../tDel.sk:13*/
        {
          if(_out_s337_1)/*../tDel.sk:13*/
          {
            BinaryTree t = tree.r;
            min = 32;
            BinaryTree tt_s449 = null;
            extractMin(t, min, tt_s449);
            BinaryTree tmp_s25_0 = tree.l;
            BinaryTree BinaryTree__s15_1 = new Branch(value=min, l=tmp_s25_0, r=tt_s449);
            _out_s333 = BinaryTree__s15_1;
          }
        }
      } // 37
      case Leaf:
      {
        bit _pac_sc_s516 = x < (tree.value);
        if(_pac_sc_s516)/*../tDel.sk:27*/
        {
          _pac_sc_s516 = 1;
        }
        if(_pac_sc_s516)/*../tDel.sk:13*/
        {
          int tmp_s169 = tree.value;
          BinaryTree BinaryTree__s97 = new Leaf(value=tmp_s169);
          _out_s333 = BinaryTree__s97;
          _has_out_ = 1;
        }
        bit _out_s375 = 0;
        if(_has_out_ == 0)/*../tDel.sk:13*/
        {
          bit _pac_sc_s516_0 = x <= (tree.value);
          if(_pac_sc_s516_0)/*../tDel.sk:27*/
          {
            _pac_sc_s516_0 = 1;
          }
          _out_s375 = _pac_sc_s516_0;
        }
        if(_has_out_ == 0)/*../tDel.sk:13*/
        {
          if(_out_s375)/*../tDel.sk:13*/
          {
            BinaryTree BinaryTree__s97_0 = new Empty();
            _out_s333 = BinaryTree__s97_0;
            _has_out_ = 1;
          }
        }
        bit _out_s375_0 = 0;
        if(_has_out_ == 0)/*../tDel.sk:13*/
        {
          _out_s375_0 = 1;
        }
        if(_has_out_ == 0)/*../tDel.sk:13*/
        {
          if(_out_s375_0)/*../tDel.sk:13*/
          {
            int tmp_s169_0 = tree.value;
            BinaryTree BinaryTree__s97_1 = new Leaf(value=tmp_s169_0);
            _out_s333 = BinaryTree__s97_1;
          }
        }
      }
      case Empty:
      {
        BinaryTree BinaryTree__s172 = new Empty();
        _out_s333 = BinaryTree__s172;
      }
    }
  }
  _out = _out_s333;
  return;
}
        
      
        
