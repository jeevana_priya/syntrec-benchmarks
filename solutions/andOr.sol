void optimize (AST original, int[4] assignment, ref AST _out)/*../andOr.sk:15*/
{
  _out = null;
  AST _out_s269 = null;
  AST AST__s5_s6 = new Var(id=2, type=1);
  AST AST__s14_s20 = new Bool(x=1);
  AST AST__s26_s27 = new Var(id=3, type=1);
  AST a_s2 = new Mux(v=AST__s5_s6, a=AST__s14_s20, b=AST__s26_s27);
  AST AST__s1 = new Not(a=a_s2);
  int[32] tmp = {1,0,8,16,0,12,16,8,4,8,20,16,4,19,0,11,0,8,20,4,16,1,24,4,4,2,18,18,17,0,8,4};
  bit _out_s271 = 0;
  check(4, AST__s1, tmp[0::4], _out_s271);
  assert (_out_s271); //Assert at ../andOr.sk:101 (-180489615921292544)
  bit _out_s273 = 0;
  check(4, AST__s1, assignment, _out_s273);
  bit _has_out_ = 0;
  if(_out_s273)/*../andOr.sk:103*/
  {
    AST AST__s105 = new Bool(x=0);
    int _out_s275 = 0;
    count(AST__s105, _out_s275);
    int _out_s277 = 0;
    count(original, _out_s277);
    assert (_out_s275 < _out_s277); //Assert at ../andOr.sk:105 (-6623236254258296765)
    _out_s269 = AST__s105;
    _has_out_ = 1;
  }
  if(_has_out_ == 0)/*../andOr.sk:108*/
  {
    _out_s269 = original;
  }
  _out = _out_s269;
  return;
}
